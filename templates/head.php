<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri() ?>/dist/fonts/font-awesome.min.css">
    <?php wp_head(); ?>
    <?php if(!empty(get_the_post_thumbnail_url())) {?>
        <style><?php
        if(is_front_page()): ?>
            .home-hero {
                background-image: url('<?php the_post_thumbnail_url() ?>');
            }<?php
        else: ?>
            .subpage-hero {
                background-image: url('<?php the_post_thumbnail_url() ?>');
            }<?php
        endif; ?>
        </style><?php
    }?>
</head>
