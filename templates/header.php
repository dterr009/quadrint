<?php
use Roots\Sage\Titles;

$theme_location = 'primary_navigation';
if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
    $menu = get_term($locations[$theme_location], 'nav_menu');
    $menu_items = wp_get_nav_menu_items($menu->term_id); ?>
    <div class="navbar-header row fixed-top justify-content-center">
        <nav class="navbar navbar-toggleable-md navbar-inverse primary-nav">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#primary_nav" aria-controls="primary_nav" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand w-20" href="/">
                <img class="header-logo" src="<?= get_stylesheet_directory_uri() ?>/dist/images/logos/Quadrint_logo.svg"/>
            </a>
            <div class="collapse navbar-collapse w-80" id="primary_nav">
                <ul class="navbar-nav ml-auto"><?php
                    foreach ($menu_items as $menu_item) {
                        if ($menu_item->menu_item_parent == 0) {
                            $parent = $menu_item->ID;
                            $menu_array = array();
                            foreach ($menu_items as $submenu) {
                                if ($submenu->menu_item_parent == $parent) {
                                    $bool = true;
                                    $menu_array[] = '<a class="dropdown-item" href="' . $submenu->url . '">' . $submenu->title . '</a>' . "\n";
                                }
                            }
                            if ($bool == true && count($menu_array) > 0) { ?>
                                <li class="nav-item dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= $menu_item->title ?><span
                                        class="caret"></span></a>
                                <div class="dropdown-menu">
                                    <?= implode("\n", $menu_array) ?>
                                </div>
                                </li><?php
                            } else { ?>
                                <li class="nav-item">
                                <a class="nav-link" href="<?= $menu_item->url ?>"><?= $menu_item->title ?></a>
                                </li><?php
                            }
                        }
                    } ?>
                </ul>
            </div>
        </nav>
    </div><?php
}

/*
 Load a custom header if we are on the front page
*/
if(is_front_page()):?>
    <div class="container-fluid home-hero" <?= is_user_logged_in() ? 'style="margin-top: -32px;"' : ''?>>
        <div class="row justify-content-center w-100 h-100 hero-overlay">
            <div class="hero-inner container row justify-content-center">
                <div class="col-12 col-md-4">
                    <div class="row justify-content-center">
                        <div>
                            <img class="hero-tagline-image" src="<?= get_stylesheet_directory_uri() ?>/dist/images/hero_tagline.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-10 col-offset-1 col-md-8 col-md-offset-0">
                    <h1 class="display-4 text-primary"><?=get_field('hero_title')?></h1>
                    <p class="lead"><?=get_field('hero_lead')?></p>
                </div>
            </div>
        </div>
    </div><?php
else: ?>
    <div class="container-fluid subpage-hero" <?= is_user_logged_in() ? 'style="margin-top: 44px;"' : ''?>>
        <div class="row justify-content-center w-100 h-100 hero-overlay">
            <div class="hero-inner container row justify-content-center">
                <div class="col-md-4 col-lg-3 subpage-hero-left">
                    <div class="row">
                        <div class="col align-self-auto">
                            <img class="hero-tagline-image" src="<?= get_stylesheet_directory_uri() ?>/dist/images/hero_tagline.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 subpage-hero-right">
                    <h1 class="h-4"><?= empty(get_field('hero_title')) ? Titles\title() : get_field('hero_title') ?></h1><?php
                    if(!empty(get_field('hero_lead'))):?>
                        <p class="lead"><?= get_field('hero_lead') ?></p><?php
                    endif; ?>
                </div>
            </div>
        </div>
    </div><?php
endif; ?>