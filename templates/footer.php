<div class="container-fluid footer">
    <div class="row justify-content-center w-100">
        <div class="container m-auto">
            <div class="row justify-content-around">
                <div class="col-12 col-lg-6 footer-left">Copyright 2017 &copy; Quadrint, Inc.  All Rights Reserved. | PRIVACY POLICY</div>
                <div class="col-12 col-lg-5 col-lg-offset-1">
                    <div class="row justify-content-around">
                        <div class="col-6">
                            <div class="row footer-icons">
                                <a href="https://www.instagram.com/quadrintinc/"><i class="fa fa-2x fa-instagram text-primary"></i></a>
                                <a href="https://www.linkedin.com/company-beta/3196569/?pathWildcard=3196569"><i class="fa fa-2x fa-linkedin text-primary"></i></a>
                                <a href="https://www.facebook.com/Quadrint/"><i class="fa fa-2x fa-facebook text-primary"></i></a>
                                <a href="https://www.google.com/maps/place/10411+Motor+City+Dr+%23375,+Bethesda,+MD+20817/@39.0279767,-77.1450413,17z/data=!3m1!4b1!4m5!3m4!1s0x89b7cc7c78212597:0x592d5bc083e3d433!8m2!3d39.0279726!4d-77.1428473">
                                    <i class="fa fa-2x fa-map-marker text-primary"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-6">
                            <?php get_search_form() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
