<?php while (have_posts()) : the_post(); ?>
    <!-- Solutions -->
    <div class="container-fluid solutions col w-100">
        <div class="container row justify-content-center m-auto">
            <div class="h2 text-primary header">Solutions</div>
        </div>
        <div class="container row justify-content-around m-auto">
            <div class="col-12 col-md-4 align-items-center solution-item">
                <div class="solution-image">
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/images/arrow_circle.png" alt="">
                </div>
                <span class="h3">Enterprise Software Solutions</span>
                <p><?= get_field('solutions_section_1') ?></p>
                <a href="#">Learn More</a>
            </div>
            <div class="col-12 col-md-4 align-items-center solution-item">
                <div class="solution-image">
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/images/triple_arrow.png" alt="">
                </div>
                <span class="h3">Agile Engineering</span>
                <p><?= get_field('solutions_section_2') ?></p>
                <a href="#">Learn More</a>
            </div>
            <div class="col-12 col-md-4 align-items-center solution-item">
                <div class="solution-image">
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/images/cloud.png" alt="">
                </div>
                <span class="h3">Cloud Integration</span>
                <p><?= get_field('solutions_section_3') ?></p>
                <a href="#">Learn More</a>
            </div>
        </div>
        <div class="container row justify-content-around m-auto">
            <div class="col-12 col-md-4 align-items-center solution-item">
                <div class="solution-image">
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/images/network.png" alt="">
                </div>
                <span class="h3">Enterprise Architecture Engineering</span>
                <p><?= get_field('solutions_section_4') ?></p>
                <a href="#">Learn More</a>
            </div>
            <div class="col-12 col-md-4 align-items-center solution-item">
                <div class="solution-image">
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/images/people.png" alt="">
                </div>
                <span class="h3">Business Analysis and Strategic Planning</span>
                <p><?= get_field('solutions_section_5') ?></p>
                <a href="#">Learn More</a>
            </div>
            <div class="col-12 col-md-4 align-items-center solution-item">
                <div class="solution-image">
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/images/graph.png" alt="">
                </div>
                <span class="h3">Data Science and Analytics</span>
                <p><?= get_field('solutions_section_6') ?></p>
                <a href="#">Learn More</a>
            </div>
        </div>
    </div>
    <!-- Solutions End -->
    <!-- Join the Team   -->
    <div class="container-fluid join-the-team w-100">
        <div class="inner-overlay row w-100 h-100">
            <div class="col-12 col-md-8 left-pane">
                <div class="h1">Join the Quadrint team</div>
                <p>At Quadrint, we take pride in the intelligence and integrity of our team. We recruit and retain experienced technology professionals with advanced knowledge, an assertive attitude and total commitment to mission success. It’s why our clients rely on Quadrint employees to get the job done.</p>
                <a href="http://quadrintinc.catsone.com/careers/">Go to Careers</a>
            </div>
            <div class="col-12 col-md-4 right-pane">
                <div class="row justify-content-center">
                    <div class="h1">Benefits</div>
                </div>
                <div id="benefits" class="benefits">
                    <div v-for="(benefit, index) in benefits" v-show="currentBenefit == index">
                        <div class="row justify-content-around">
                            <div @click="nextSlide()"><i class="fa fa-chevron-left"></i></div>
                            <img class="benefit-image" :src="'<?= get_stylesheet_directory_uri() ?>/dist/images/'+benefit.image" alt="">
                            <div @click="prevSlide()"><i class="fa fa-chevron-right"></i></div>
                        </div>
                        <div class="row justify-content-center">
                            <p style="text-align: center;">{{ benefit.text }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Join the Team end    -->
    <!-- Insights   -->
    <div class="insights">
        <div class="container m-auto row justify-content-center">
            <span class="h1 text-primary">Insights</span>
        </div>
        <div class="container m-auto row justify-content-around"><?php
            // get the 3 latest posts
            global $post;
            $args = ['orderby' => 'date', 'order' => 'DESC', 'numberposts' => 3];
            $posts = get_posts( $args );
            foreach ( $posts as $post ) : setup_postdata( $post ); ?>
                <div class="col-12 col-md-4 insight">
                    <div class="text-primary"><?php the_date()?></div>
                    <div class="h5"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                    <p><?php the_excerpt(); ?></p>
                </div><?php
            endforeach;
            wp_reset_postdata(); ?>
        </div>
        <!--<div class="m-auto row justify-content-center insight-footline">
            <span class="h4">WANT MORE INSIGHTS FROM QUADRINT?</span>
            <a class="h4 text-primary" href="#">CLICK HERE</a>-->
        </div>
    </div>
    <!-- Insights End -->
    <!-- Quote Area -->
    <div class="quote-area">
        <div class="inner-overlay h-100">
            <div class="container col-12">
                <div class="quote m-auto row justify-content-center">
                    <span class="h1 text-success">&quot;We believe that serving the community is as essential as serving our clients&quot;</span>
                </div>
                <div class="row m-auto justify-content-center">
                    <span class="h3" style="color: white;">Glenn Merberg, Managing Principal</span>
                </div>
            </div>
        </div>
    </div>
    <!-- Quote Area End   -->
    <!-- Community   -->
    <div class="container m-auto community">
        <div class="row justify-content-center">
            <span class="h1 text-primary">The Community We Serve</span>
        </div>
        <div class="row justify-content-around">
            <div class="col-10 col-offset-1 col-lg-2 col-md-6">
                <div class="row justify-content-center">
                    <div><img src="<?= get_stylesheet_directory_uri() ?>/dist/images/logos/cf_foundation.png" alt=""></div>
                </div>
            </div>
            <div class="col-10 col-offset-1 col-lg-2 col-md-6">
                <div class="row justify-content-center">
                    <div><img src="<?= get_stylesheet_directory_uri() ?>/dist/images/logos/best_kids.png" alt=""></div>
                </div>
            </div>
            <div class="col-10 col-offset-1 col-lg-2 col-md-6">
                <div class="row justify-content-center">
                    <div><img src="<?= get_stylesheet_directory_uri() ?>/dist/images/logos/wounded_warrior.png" alt=""></div>
                </div>
            </div>
            <div class="col-10 col-offset-1 col-lg-2 col-md-6">
                <div class="row justify-content-center">
                    <div><img src="<?= get_stylesheet_directory_uri() ?>/dist/images/logos/lls_logo.jpg" alt=""></div>
                </div>
            </div>
            <div class="col-10 col-offset-1 col-lg-2 col-md-6">
                <div class="row justify-content-center">
                    <div><img src="<?= get_stylesheet_directory_uri() ?>/dist/images/logos/human_rescue_alliance.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Community End   -->
    <?php //get_template_part('templates/content', 'page'); ?>
<?php endwhile;

$benefits = [
    ['text' => '50% of base health and dental', 'image' => 'health_2x.png'],
    ['text' => 'Retirement Plan', 'image' => 'retirement.svg'],
    ['text' => 'Career Advancement Training', 'image' => 'training.svg'],
    ['text' => 'Vacation and Schedule Flexibility', 'image' => 'vacation.svg'],
    ['text' => 'Recruiting Incentives', 'image' => 'recruitment.svg']
];?>

<script src="<?= get_template_directory_uri() ?>/dist/scripts/vue.min.js"></script>
<script>
    var data = <?= json_encode($benefits)?>;
    var benefits = new Vue({
      el: '#benefits',
      data: {
        benefits: data,
        currentBenefit: null,
        sliderInterval: null
      },
      created: function() {
        this.currentBenefit = 0;
      },
      mounted: function() {
        this.sliderInterval = setInterval(function() { this.play() }.bind(this), 4000);
      },
      methods: {
        play: function() {
          if(this.currentBenefit == this.benefits.length - 1)
            this.currentBenefit = 0;
          else
            this.currentBenefit++;
        },
        nextSlide: function() {
          window.clearInterval(this.sliderInterval);
          if(this.currentBenefit == this.benefits.length - 1)
            this.currentBenefit = 0;
          else
            this.currentBenefit++;
//          setTimeout(function () { this.nextSlide() }.bind(this), 4000)
        },
        prevSlide: function () {
          window.clearInterval(this.sliderInterval);
          if(this.currentBenefit == 0)
            this.currentBenefit = this.benefits.length - 1;
          else
            this.currentBenefit--;
        }
      }
    });
</script>