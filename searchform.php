<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="row">
        <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
        <div class="col-12 col-lg-8">
            <input class="form-control" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Search Quadrint"/>
        </div>
        <div class="col-12 col-lg-4">
            <input class="btn btn-primary" type="submit" id="searchsubmit" value="<?php echo esc_attr_x('Search', 'submit button'); ?>"/>
        </div>
    </div>
</form>